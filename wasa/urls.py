from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

from tastypie.api import Api

from locality.api import LocationResource
#from report.api import Report , Category

v1_api = Api(api_name='v1')
v1_api.register(LocationResource())


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'wasa.views.home', name='home'),
    # url(r'^wasa/', include('wasa.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
     url(r'^grappelli/', include('grappelli.urls')),
     url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
     url(r'^admin/', include(admin.site.urls)),
     url(r'^report/' , include('report.urls' , namespace='report')),
     
     url(r'^api/', include(v1_api.urls)),
)
