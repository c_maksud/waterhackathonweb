from report.models import Report , Category
from django.contrib import admin

class ReportAdmin(admin.ModelAdmin):
    list_display = ('location' ,'category' , 'get_compact','is_processing','is_solved',
                        #'status',
                        # 'type',
                        #'total_price','area_size','price_per_area',#'numeric_total_price','submission_date',
                        )
    
    """    
    search_fields = (
                           'name','location__name',# 'total_price', 'developer__name',
                           'property_id','lift', 'parking'
                         )
    """
    list_filter = (
                        'is_processing' , 'is_solved' , 'category','location',
                  )
        

admin.site.register(Report , ReportAdmin)
admin.site.register(Category)

