# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Report.description'
        db.add_column(u'report_report', 'description',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Report.publish_date'
        db.add_column(u'report_report', 'publish_date',
                      self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 2, 14, 0, 0)),
                      keep_default=False)

        # Adding field 'Report.completion_date'
        db.add_column(u'report_report', 'completion_date',
                      self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Report.is_processing'
        db.add_column(u'report_report', 'is_processing',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Report.is_solved'
        db.add_column(u'report_report', 'is_solved',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Report.description'
        db.delete_column(u'report_report', 'description')

        # Deleting field 'Report.publish_date'
        db.delete_column(u'report_report', 'publish_date')

        # Deleting field 'Report.completion_date'
        db.delete_column(u'report_report', 'completion_date')

        # Deleting field 'Report.is_processing'
        db.delete_column(u'report_report', 'is_processing')

        # Deleting field 'Report.is_solved'
        db.delete_column(u'report_report', 'is_solved')


    models = {
        u'report.category': {
            'Meta': {'object_name': 'Category'},
            'category_id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'report.report': {
            'Meta': {'object_name': 'Report'},
            'category_id': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['report.Category']", 'null': 'True', 'blank': 'True'}),
            'completion_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_processing': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_solved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'publish_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 2, 14, 0, 0)'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['report']