import os
import json

from django.http import HttpResponse

from locality.models import Location
from django.conf import settings

def load_location(request):
    JSON_PATH = os.path.join(settings.PROJECT_PATH , 'property_location.json')
    json_file = open(JSON_PATH , 'r')
    json_string = json_file.read()
    json_string = json_string.replace('\r', '\\r').replace('\n', '\\n')
    
    json_objs = json.loads(json_string)
    
    for json_obj in json_objs:
        location = Location()
        location.name = json_obj.get('name')
        location.district = None
        location.save()
        
    return HttpResponse(content = json_string ,  content_type='application/json')
        

    
    
    
    