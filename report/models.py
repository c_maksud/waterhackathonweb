from django.db import models
from datetime import datetime
from django.contrib.auth.models import User
from locality.models import Location
from django.template.defaultfilters import floatformat , truncatewords , striptags , truncatewords_html

class Category(models.Model):
    category_id = models.AutoField(primary_key = True , blank=True)
    value = models.CharField(max_length = 200 , null=True , blank=True)
    
    def __unicode__(self):
        return self.value

class Report(models.Model):
    user = models.ForeignKey(User)
    location = models.ForeignKey(Location)
    title = models.CharField(max_length=200 , null=True , blank=True)
    category = models.ForeignKey(Category , null=True , blank=True)
    description  = models.TextField(null=True , blank=True)
    
    publish_date = models.DateTimeField(default = datetime.now() )
    completion_date = models.DateTimeField(null=True , blank=True)
    
    is_processing = models.BooleanField(default=False)
    
    is_solved = models.BooleanField(default=False)
    
    longitude = models.FloatField(null=True , blank=True)
    latitude = models.FloatField(null=True , blank=True)
    
    def __unicode__(self):
        return truncatewords(self.description , 10)
    
    def get_compact(self):
        return self.__unicode__()
    
    def duration(self):
        if self.is_solved:
            return self.completion_date - self.publish_date
        else:
            return None
        
    
    
    
    
        
    