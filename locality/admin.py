from django.contrib import admin

from locality.models import Country , District , Location

admin.site.register(Location)
admin.site.register(District)
admin.site.register(Country)