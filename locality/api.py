from tastypie.resources import ModelResource

from locality.models import Country , District , Location


class LocationResource(ModelResource):
    class Meta:
        queryset = Location.objects.all()
        resource_name = 'location'