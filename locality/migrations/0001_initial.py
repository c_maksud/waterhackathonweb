# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Country'
        db.create_table(u'locality_country', (
            ('country_id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
        ))
        db.send_create_signal(u'locality', ['Country'])

        # Adding model 'District'
        db.create_table(u'locality_district', (
            ('district_id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['locality.Country'])),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
        ))
        db.send_create_signal(u'locality', ['District'])

        # Adding model 'Location'
        db.create_table(u'locality_location', (
            ('location_id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('district', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['locality.District'], null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=200)),
        ))
        db.send_create_signal(u'locality', ['Location'])


    def backwards(self, orm):
        # Deleting model 'Country'
        db.delete_table(u'locality_country')

        # Deleting model 'District'
        db.delete_table(u'locality_district')

        # Deleting model 'Location'
        db.delete_table(u'locality_location')


    models = {
        u'locality.country': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Country'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'country_id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'})
        },
        u'locality.district': {
            'Meta': {'ordering': "('name',)", 'object_name': 'District'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['locality.Country']"}),
            'district_id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'})
        },
        u'locality.location': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Location'},
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['locality.District']", 'null': 'True', 'blank': 'True'}),
            'location_id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '200'})
        }
    }

    complete_apps = ['locality']