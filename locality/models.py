from django.db import models

class Country(models.Model):
    country_id = models.AutoField(primary_key=True , blank=True)
    code = models.CharField(max_length=100 , null=True , blank=True)
    name=  models.CharField(max_length=100 , unique = True)
    
    class Meta:
        ordering = ('name' , ) 
    
class District(models.Model):
    district_id = models.AutoField(primary_key=  True , blank=True)
    country = models.ForeignKey('Country')
    name = models.CharField(max_length=100 , unique = True)
    
    class Meta:
        ordering = ('name' , ) 


class Location(models.Model):
        location_id = models.AutoField(primary_key=True , blank=True)
        district  = models.ForeignKey('District' , null=True , blank=True)
        name = models.CharField(max_length = 200, unique = True)    
        
        class Meta:
            ordering = ('name' , )    
        
        def __unicode__(self):
            return self.name
